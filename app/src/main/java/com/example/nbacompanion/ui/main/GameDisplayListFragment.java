package com.example.nbacompanion.ui.main;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nbacompanion.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * A placeholder fragment containing a simple view.
 */
public class GameDisplayListFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String LAYOUT_NAME = "layout_name";
    private static final String LAYOUT_LINEAR = "linear_layout";
    private static final String LAYOUT_GRID = "grid_layout";
    public static final String GAMEID_KEY = "gameID";

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fab;
    private View thisView ;
    private int index;
    private String fragment_title;



    public static GameDisplayListFragment newInstance(int index, String title) {
        GameDisplayListFragment fragment = new GameDisplayListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.index = index;
        fragment.fragment_title = title;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (getArguments() != null) {
            this.index = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        if(savedInstanceState != null){
            CharSequence layoutType = savedInstanceState.getCharSequence(LAYOUT_NAME);
            if(layoutType != null && layoutType.length() > 0){
                if(layoutType.equals(LAYOUT_GRID)){
                    layoutManager = new GridLayoutManager(getContext(), 2);
                }else{
                    layoutManager = new LinearLayoutManager(getContext());
                }
            }

        }

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        thisView = root;

        TextView titleTextView = root.findViewById(R.id.fragment_main_title);
        titleTextView.setText(this.fragment_title);

        //Getting current date to request api for the games of the day
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar myCalendar = Calendar.getInstance();
        String currentDate = sdf.format(myCalendar.getTime());

        switch (index){
            case 1:

                break;
            case 2:
                break;


        }
        return root;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(recyclerView.getLayoutManager() instanceof LinearLayoutManager){
            outState.putCharSequence(LAYOUT_NAME,LAYOUT_LINEAR);
        }
        if(recyclerView.getLayoutManager() instanceof  GridLayoutManager){
            outState.putCharSequence(LAYOUT_NAME,LAYOUT_GRID);
        }
        //I decided not to save the list of games so i could use the fragment lifecycle as a "data refresher" (forcing an api call to get it )
        super.onSaveInstanceState(outState);

    }



}

